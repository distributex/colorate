import * as firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/firestore';
import firebaseConfig from './.firebaseConfig.json';


const categories = ['magenta', 'pink', 'red', 'orange', 'yellow', 'green', 'cyan', 'blue'];

const store = {
  state: {
    user: {
      name: null,
      sex: null,
      id: null,
      age_group: null,
    },
    users: {}
  },

  init() {
    var config = firebaseConfig;
    firebase.initializeApp(config);
    this.db = firebase.firestore();
    const settings = {timestampsInSnapshots: true};
    this.db.settings(settings);
    try {
      if (localStorage.getItem("state")) {
        this.state = JSON.parse(localStorage.getItem("state"))
      }
    } catch(e) {
      console.error("store.js::init", e)
    }

    console.log('store.js::init', this.state);
  },
  async save() {
    console.log("store.js::save")
    localStorage.setItem("state", JSON.stringify(this.state))
  },
  async add(event_id, color) {
    return await this.db.collection("colors").add({
      user_id: this.state.user_id,
      event_id: event_id,
      color: color
   })
  },
  async signup(name, age_group, sex) {
    const docRef = await this.db.collection("users").add({
      name: name,
      age_group: age_group,
      sex: sex
    })
    this.state.user.name = name;
    this.state.user.sex = sex;
    this.state.user.id = docRef.id;
    ga('send', 'event', 'user', 'signup', 'name', name)
    ga('send', 'event', 'user', 'signup', 'sex', sex)
    ga('send', 'event', 'user', 'signup', 'age_group', age_group)
            
    await this.save()
  },
  async createEvent(eventGroup, eventName) {
    const docRef = await this.db.collection("events").add({
      name: eventName,
      group: eventGroup
    })
    return docRef.id
  },
  async getEvents(group) {
    const querySnapshot = await this.db.collection("events").where("group", "==", group).get();
    const items = [];
    querySnapshot.forEach(doc => {
      items.push({id: doc.id, ...doc.data()});
    });

    items.sort((a, b) => {
      if (a.order && b.order) {
        return a.order - b.order;
      } else {
        return 0
      }
    })

    console.log("store.js::getEvents", items);
    return items;
  },
  async updateUsersCache() {
    const querySnapshot = await this.db.collection("users").get();
    const items = {};
    querySnapshot.forEach(doc => {
      items[doc.id] = doc.data();
    });
    console.log("store.js::updateUsersCache", items);
    this.state.users = items;
    return items;
  },
  getUser(id) {
    return this.state.users[id];
  },
  async getEvent(id) {
    const doc = await this.db.collection("events").doc(id).get();
    console.log("store.js::getEvent: ", id, doc.exists)
    if (doc.exists) {
      return {id: doc.id, ...doc.data()}
    } else {
      null
    }
  },
  async getColors(eventId) {
    const querySnapshot = await this.db.collection("colors").where("event_id", "==", eventId).get();
    const items = [];
    querySnapshot.forEach(doc => {
      items.push({id: doc.id, ...doc.data()});
    });
    console.log("store.js::getColors", items);
    return items;
  },
  async submitColor(eventId, color) {
    if (!this.state.user.id) {
      throw Error("User ID is not specified")
    }

    const doc = await this.db.collection("colors").add({
      color: color,
      event_id: eventId,
      user_id: this.state.user.id
    });
    console.log("store.js::submitColor: ", eventId, color, doc.id)
    ga('send', 'event', 'color', 'submit', eventId, color);
    return doc.id;
  },
  extractSeriesForUser(colors, sex) {
    const obj = {}

    for (const cat of categories) {
      obj[cat] = 0;
    }

    for (let color of colors) {
      const user = this.getUser(color.user_id)
      if (user.sex == sex) {
        obj[color.color] += 1;
      }
    }

    const series = [];
    for (const cat of categories) {
      series.push(obj[cat])
    }
  
    return series;
  },
  extractSeries(colors) {
    const obj = {}

    for (const cat of categories) {
      obj[cat] = 0;
    }

    for (let color of colors) {
      obj[color.color] += 1; 
    }

    const series = [];
    for (const cat of categories) {
      series.push(obj[cat])
    }
  
    return series;
  },

  buildHistogram(colors) {
    const data = {
      title: {
        text: ''
      },
      xAxis: {
        categories: categories,
        labels: {
          formatter () {
            return `<span style="color: ${this.value}; font-size: 28px;">${this.value}</span>`
          }    
        }
      },
      yAxis: {
        
      },
      chart: {
        type: 'column'
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        }
      },
      series: [
        {
          name: 'all',
          color: 'gray',
          data: this.extractSeries(colors)
        },
        // {
        //   name: 'male',
        //   color: 'blue',
        //   data: this.extractSeriesForUser(colors, 'male')
        // },
        // {
        //   name: 'female',
        //   color: 'pink',
        //   data: this.extractSeriesForUser(colors, 'female')
        // },
        // {
        //   name: 'unknown',
        //   color: 'black',
        //   data: this.extractSeriesForUser(colors, 'unknown')
        // }
      ],
      legend: {
        itemStyle: {
            color: '#000000',
            // fontWeight: 'bold',
            fontSize: '15px'
        }
      }
    };
    console.log('store.js::buildHistogram', data)
    return data;
  }
};

store.init();

export default store;