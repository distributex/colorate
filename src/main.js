import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'
import VueAnalytics from 'vue-analytics'
import VueHighcharts from 'vue-highcharts';

import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(Vuetify)
Vue.use(VueHighcharts);

import App from './App.vue';
import Colorate from './components/Colorate.vue';
import Signup from './components/Signup.vue'
import Events from './components/Events.vue';
import Pay from './components/Pay.vue';
import Payments from './components/Payments.vue';
import Statistics from './components/Statistics.vue';

import store from './store.js'

const routes = [
  { path: '/signup', component: Signup },
  { path: '/colorate/:id', component: Colorate, meta: {auth: true} },
  { path: '/events/:group', component: Events, meta: {auth: true} },
  { path: '/pay/:id', component: Pay, meta: {auth: true}},
  { path: '/payments', component: Payments, meta: {auth: true} },
  { path: '/', component: Payments, meta: {auth: true} },
  { path: '/stat/:id', component: Statistics }
]

const router = new VueRouter({routes});
router.beforeEach((to, from, next) => {
  console.log("main.js::beforeEach", to, from)
  const authRequired = to.matched.some((route) => route.meta.auth)
  const authed = store.state.user.id
  if (authRequired && !authed) {
    next(`/signup/?redirect=${to.path}`)
  } else {
    next()
  }
})

Vue.use(VueAnalytics, {
  id: 'UA-113907909-3',
  router
})


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
