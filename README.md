# Colorate App Demo

Team ColoRate are developing an emotion evaluating software that records user color feedback data and connects it with personal data to help business to understand their customers better and propose more personalized service.

![Payment Demo](demo/payment.gif)

![Payment Demo](demo/mood.gif)

![Payment Demo](demo/hackhathon.gif)

![Payment Demo](demo/stat.gif)

![Payment Demo](demo/colorate-payment.jpg)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

